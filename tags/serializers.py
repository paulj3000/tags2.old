from rest_framework import serializers
from rest_framework import fields
from tags.models import Tag
from accounts.models import User
from pprint import pprint


class TagSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tag
        fields = ('id', 'tag_name',)

    '''
    def get_url(self, obj):
        request = self.context['request']
        return {'self': reverse('event', kwargs={'eventid': obj.pk}, request=request)}

    def get_advice(self, data):
        advice = Advice.objects.filter(event=data)
        return AdviceSerializer(advice, many=True, context=self.context).data
    '''

'''
class EventUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventUserInfo
        fields = ('user_id', 'display_name', 'relation',)


class AdviceTextSerializer(serializers.ModelSerializer):
    text = serializers.SerializerMethodField()

    def get_text(self, data):
        return data.advice_text

    class Meta:
        model = AdviceText
        fields = ('text',)


class AdviceVideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdviceVideoData
        fields = ('mp4_url', 'thumbnail_url',)


class AdviceSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()
    likes = serializers.SerializerMethodField()
    liked = serializers.SerializerMethodField()
    advice = serializers.SerializerMethodField()
    type = serializers.SerializerMethodField()

    def get_likes(self, data):
        return data.get_likes_count()

    def get_liked(self, data):
        request = self.context['request']
        return data.is_liked_by_user(request.user)

    def get_type(self, data):
        return 'text' if hasattr(data, 'advicetext') else 'video'

    def get_user(self, data):
        eui = EventUserInfo.objects.get(event=data.event, user=data.user)
        return EventUserSerializer(eui, many=False, context=self.context).data

    def get_advice(self, data):
        if data.is_text():
            return AdviceTextSerializer(data.advicetext, many=False, context=self.context).data
        elif data.is_video():
            video_data = data.advicevideo.advicevideodata
            return AdviceVideoSerializer(video_data, many=False, context=self.context).data

    class Meta:
        model = Advice
        fields = ('id', 'user', 'created_date', 'event_id', 'anonymous', 'likes', 'liked', 'advice', 'type',)
'''
