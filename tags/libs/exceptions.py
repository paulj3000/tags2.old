class DuplicateTagException(Exception):
    ''' 
    handle the instance where the tag has already
    been created
    '''
    pass
