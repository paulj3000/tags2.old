# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from tags.models import *

# Register your models here.
admin.site.register(Tag)
admin.site.register(Category)
