# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from utils.models import uuidmodel
from .libs.exceptions import DuplicateTagException

TAG_TYPE_PRIVATE = 0
TAG_TYPE_ENTERPRISE = 1
TAG_TYPE_CAMPAIGN = 2
TAG_TYPE_PUBLIC = 3

TAG_TYPES = (
    (TAG_TYPE_PRIVATE, 'PRIVATE'),
    (TAG_TYPE_ENTERPRISE, 'ENTERPRISE'),
    (TAG_TYPE_CAMPAIGN, 'CAMPAIGN'),
    (TAG_TYPE_PUBLIC, 'PUBLIC')
)


class Category(uuidmodel.Model):
    title = models.CharField(max_length=100, unique=True, db_index=True)

    def __unicode__(self):
        return self.title

    class Meta:
        db_table = 'category'
        verbose_name_plural = 'categories'


class TagManager(models.Manager):
    def get_or_create(self, *args, **kwargs):
        kwargs['tag_name'] = kwargs['tag_name'].upper()
        return super(TagManager, self).get_or_create(*args, **kwargs)

    def create(self, *args, **kwargs):
        # Now call the super method which does the actual creation

        # search for a tag based on the name

        tag_name = kwargs.get('tag_name').upper()
        tag = Tag.objects.filter(tag_name__iexact=tag_name).first()
        if tag:
            if kwargs.get('tag_type') == TAG_TYPE_PUBLIC and \
                tag.tag_type != TAG_TYPE_PUBLIC:
                    tag.tag_type = PUBLIC_TAG;
                    tag.save();

            # raise our error
            raise DuplicateTagException(tag_name)

        # now create the tag and return
        return super(TagManager, self).create(*args, **kwargs)

    def set_public(self, tag_ids):
        Tag.objects.filter(id__in=tag_ids).update(tag_type=TAG_TYPE_PUBLIC)

    def get_tags_by_id(self, tag_ids):
        return Tag.objects.filter(id__in=tag_ids)

    def get_tags_by_name(self, tag_names):
        return Tag.objects.filter(tag_name__in=tag_names)

    def find_tag(self, tag_name, tag_type):
        Tag.objects.filter(tag_name=tag_name, tag_type=tag_type).first()

    '''
      /**
       * Get all tags - the parameters are combined in query builder fashion.
       *
       * TODO: This should probably filter out public tags, for privacy reasons.
       * @param tag               Optional tag name
       * @param type              Optional tag type
       * @param suggestionOptions Optional
       * @param pageSize
       * @param skip
       * @param orderByField
       * @param reverseSort
       * @param callback
       */
    '''
    '''
    def get_all_tags(self, tag, type, suggestionOptions, pageSize, skip, orderByField, reverseSort):
        var searchOrArray = [],
            searchAndArray = [],
            searchQuery = {},
            tagSearch = {tag: { '$regex': '^' + tag.trim(), $options: 'i'}},
            typeSearch = {type: { '$regex': '^' + type, $options: 'i'}};

        //suggestionOptions
        //0 - No filter
        //1 - Regular/Non-suggested
        //2 - suggested

        if(tag && tag.length > 0){
            searchOrArray.push(tagSearch);
        }

        if(type && type.length > 0){
            searchOrArray.push(typeSearch);
        }

        if(searchOrArray.length > 0){
            if(suggestionOptions && (suggestionOptions != "0")) {
                searchAndArray.push({isSuggested: (suggestionOptions == 2)});
            }
            searchAndArray.push({$or: searchOrArray});
            searchQuery = {$and: searchAndArray}
        }
        else if(suggestionOptions && (suggestionOptions != "0")){
            searchQuery = {isSuggested: (suggestionOptions == 2)};
        }

        var fieldsToReturn = '_id tag type isSuggested category searchCount';

        var sort_order = {};
        sort_order[orderByField] = reverseSort;

        //In the current version of Mongo we have to run 2 queries to get the count and the data
        async.parallel([
                function(callback){
                    Tag.count(searchQuery, function(err, count){
                        callback(err, count);
                    });
                },
                function(callback){
                    Tag.find(searchQuery, fieldsToReturn, {sort: sort_order, skip: skip, limit: pageSize })
                        .lean()
                        .exec(function(err, tags){
                            callback(err, tags);
                        });
                }
            ],
            //finally
            function(err, results){
                var tags = results && results[1] && results[1].map(function(f)
                {
                    if (f.category && f.isSuggested)
                        f.categoryDisplay   = TAGS_CATEGORY[f.category];
                    else
                        f.categoryDisplay   = '';

                    return f;
                });

                callback(err, {count: results[0], tags: tags });
            }
        );
    }
    '''




class Tag(uuidmodel.Model):
    from accounts.models import User

    user = models.ForeignKey(User)
    tag_name = models.CharField(max_length=100, unique=True, db_index=True)
    tag_type = models.PositiveSmallIntegerField(choices=TAG_TYPES)
    blocked = models.BooleanField(default=False)
    is_suggested = models.BooleanField(default=False)
    search_count = models.PositiveSmallIntegerField(default=0)
    category = models.ForeignKey(Category, null=True)
    is_active = models.BooleanField(default=True)

    objects = TagManager()

    class Meta:
        db_table = 'tag'
        verbose_name_plural = 'tags'

    def __unicode__(self):
        return self.tag_name

    def up_search_counter(self):
        self.set_attribute('search_count', self.search_count +1)

    def set_active(self, flag):
        self.set_attribute('is_active', flag)

    def set_attribute(self, field, value):
        setattr(self, field, value)
        self.save()
