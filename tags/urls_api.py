from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
import tags.views.api as views_api

urlpatterns = [
    url(r'^$', views_api.TagsList.as_view()),
]
