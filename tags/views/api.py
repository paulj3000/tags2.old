from pprint import pprint

from rest_framework import viewsets
from rest_framework.decorators import detail_route
from rest_framework import generics, permissions, renderers
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import mixins
from rest_framework import generics


#from events.lib.decorators import verify_can_access_event, can_access_advice
from tags.serializers import TagSerializer
from tags.models import Tag
#from events.models import Event, Advice
#from accounts.models import User

#from utils.api.acauthentication import ACAuthentication

class TagsList(generics.ListCreateAPIView):
    serializer_class = TagSerializer

    def list(self, request):
        #self.user = request.user
        queryset = self.get_queryset()
        serializer = TagSerializer(queryset, many=True)
        return Response({'tags': serializer.data})

    def get_queryset(self):
        return Tag.objects.all()
        #return Tag.objects.get_user_events(self.user)

'''
class EventDetail(generics.RetrieveUpdateDestroyAPIView):
    lookup_url_kwarg = 'eventid'
    queryset = Event.objects.all()
    serializer_class = EventSerializer

    def get_serializer_context(self):
        return {"show_advice": True}

    def get_queryset(self):
        user = self.request.user
        return Event.objects.all()


class EventHighlight(generics.GenericAPIView):
    lookup_url_kwarg = 'eventid'
    queryset = Event.objects.all()
    renderer_classes = (renderers.StaticHTMLRenderer,)

    def get(self, request, *args, **kwargs):
        snippet = self.get_object()
        return Response(snippet.highlighted)


class AdviceList(generics.ListCreateAPIView):
    #queryset = Event.objects.all()
    serializer_class = AdviceSerializer

    def get_queryset(self):
        pprint(self.kwargs)
        print Advice.objects.filter(event_id=self.kwargs['eventid'])
        return Advice.objects.filter(event_id=self.kwargs['eventid'])


class AdviceDetail(generics.RetrieveAPIView):
    lookup_url_kwarg = 'adviceid'
    authentication_classes = (ACAuthentication, )
    serializer_class = AdviceSerializer

    def get_queryset(self):
        adviceid = self.kwargs.get('adviceid')
        return Advice.objects.filter(id=adviceid)
'''
