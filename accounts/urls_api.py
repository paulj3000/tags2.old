from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
import accounts.views.api as views_api
import accounts.api.tags as api_tags
import accounts.api.users as api_users

urlpatterns = [

    url(r'^create$', views_api.CreateUser.as_view()),
    url(r'^login$', views_api.LoginUser.as_view()),

    # do some usesr stuff
    url(r'^(?P<userid>[\w]+)/profile(?P<mode>(image|background))$', api_users.UploadProfileImage.as_view()),

    # do some tag stuff 
    url(r'^assigntag$',api_tags.AssignTagToUser.as_view()),
]
