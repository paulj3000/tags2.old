# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from accounts.libs.exceptions import *

from tags2.settings import (
    STATUS_NOTREGISTERED, STATUS_SUSPENDED, STATUS_CONFIRMED,
    STATUS_CURRENT, STATUS_DEACTIVATED,
    STATUS_BLOCKED, STATUS_DEACTIVATING, US_STATES
)


USER_STATUS = (
    (STATUS_NOTREGISTERED, 'Not Registered'),
    (STATUS_CURRENT, 'Current'),
    (STATUS_SUSPENDED, 'Suspended'),
    (STATUS_DEACTIVATED, 'Deactivated'),
    (STATUS_DEACTIVATING, 'Deactivating'),
    (STATUS_BLOCKED, 'Blocked'),
)

USER_PROFILE_IMAGE_IMAGE = 0
USER_PROFILE_IMAGE_BACKGROUND = 0

class UserManager(BaseUserManager):

    def create_user(self, username, email, password=None, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')

        now = timezone.now()
        email = self.normalize_email(email)
        user = self.model(email=email, username=username, last_login=now, date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password, **extra_fields):
        ''' override the create superuser function '''
        user = self.create_user(username, email, password=password)
        user.is_admin = True
        user.save(using=self._db)
        return user

    def user_has_tag(self, user_id, assigned_by_user_id, tag_name, tag_type):
        return UserTag.objects.filter(
            user_id=user_id,
            assigned_by_user_id=assigned_by_user_id,
            tag__tag_name=tag_name.upper(),
            tag__tag_type=tag_type
        ).count()


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=255, unique=True, db_index=True)
    email = models.EmailField(max_length=255, unique=True, db_index=True)
    first_name = models.CharField(max_length=100, blank=True)
    last_name = models.CharField(max_length=100, blank=True)
    phone = models.CharField(max_length=100, blank=True)
    phone_key = models.CharField(max_length=100, blank=True)
    device_id = models.CharField(max_length=100, blank=True)
    no_sms = models.BooleanField(default=True)

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    status = models.IntegerField(blank=True, choices=USER_STATUS, default=STATUS_NOTREGISTERED)

    objects = UserManager()

    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    last_login_date = models.DateTimeField(null=True)

    USERNAME_FIELD = 'username'

    REQUIRED_FIELDS = ['email',]

    class Meta:
        db_table = 'user'

    @property
    def is_staff(self):
        return self.is_admin

    def get_full_name(self):
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def get_abbreviated_name(self):
        return "%s %s." % (self.first_name, self.last_name[0])

    def __unicode__(self):
        return self.get_full_name()

    def set_sms(self, flag):
        self.no_sms=flag
        self.save()

    # -----------------------------------------------------------------------------
    # Add some tag management
    # -----------------------------------------------------------------------------
    def add_tag(self, assigned_by_user, tag_name, tag_type):
        was_added_by_different_user = self != assigned_by_user

        tag_name = tag_name.upper()

        user_tag = self.user_tags.filter(
            assigned_by_user=assigned_by_user,
            tag__tag_name=tag_name,
            tag__tag_type=tag_type
        ).first()

        if user_tag:
            raise DuplicateAssignedUserTagException(assigned_by_user, tag_name, tag_type)

        return UserTag.objects.assign_tag_to_user(self, assigned_by_user, tag_name, tag_type)

    def has_tag(self, assigned_by_user, tag_name, tag_type):
        return self.user_tags.filter(
            assigned_by_user=assigned_by_user,
            tag__tag_name=tag_name,
            tag__tag_type=tag_type
        ).count()

    def remove_tag(self, tag_name, tag_type):
        from tags.models import Tag
        tag = Tag.objects.get(tag_name=tag_name, tag_type=tag_type)

        # get the user tag object
        user_tag = self.user_tags.objects.filter(tag=tag)

        if not user_tag:
            raise InvalidUserTagException("tag %s is not assigned to user" % tag_name)

        user_tag.delete()
        UserTagDeleted.objects.create(user=self, tag=tag)

    # -----------------------------------------------------------------------------
    # Add some functions for image uploading
    # -----------------------------------------------------------------------------
    def upload_profile_image(self, content_type, image_type, image):
        from utils.aws.s3 import S3
        import base64
        import time

        s3 = S3('tags2.dev')

        ACCEPTED_CONTENT_TYPES = {
            'image/png': 'png',
            'image/gif': 'gif',
            'image/jpeg': 'jpg',
            'image/jpg': 'jpg',
        }

        if content_type not in ACCEPTED_CONTENT_TYPES:
            raise InvalidContentTypeException(content_type)
            
        # convert the image back from base64 
        image = base64.b64decode(image)

        filename = 'accounts/%s/gallery/%s.%s' % (
            self.id,
            int(time.time()),
            ACCEPTED_CONTENT_TYPES[content_type]
        )

        s3.upload_file(filename, image, content_type=content_type)

        # now update / create the user profile image stuff
        UserProfileImage.objects.update_or_create(
            user=self,
            image_type=image_type,
            defaults={'image': filename},
        )

        # and of course return
        return filename

    def set_profile_image(self, content_type, image):
        return self.upload_profile_image(content_type, USER_PROFILE_IMAGE_IMAGE, image)

    def set_profile_background(self, content_type, image):
        return self.upload_profile_image(content_type, USER_PROFILE_IMAGE_BACKGROUND, image)


    '''
  function addContact(user1Id, user2Id, callback) {
    if (user1Id == user2Id) {
      // we cannot add ourselves.
      callback({error: 'A user cannot add himself', type: 400}, null);
      return;
    }

    async.parallel([
        function (callback) {
          User.findById(user1Id, '_id userName phoneKey contacts', function (err, user1) {
            if (!user1) {
              callback({message: "User was not found.", type: 404}, null);
            } else {
              callback(err, user1);
            }
          });
        },
        //We are loading the contact just to ensure that it's valid.
        function (callback) {
          User.findById(user2Id, '_id', function (err, user2) {
            if (!user2) {
              callback({message: "User was not found.", type: 404}, null, null);
            } else {
              callback(err, user2);
            }
          });
        }],
      //After the users have been found
      function (err, results) {
        var user1 = results[0];

        //Stop if there was an error
        if (err) {
          callback(err, null);
          return;
        }

        async.parallel([
            function (callback) {
              addOneWayContact(user1, user2Id, callback);
            }
          ],
          function (err, results) {
            var user1 = results[0];

            //Async returns undefined for no errors, but our code expects null
            if (typeof err === 'undefined') {
              err = null;
            }

            callback(err, user1);
          });
      });
  }
    '''

class UserContact(models.Model):
    user = models.ForeignKey(User, related_name='user_contacts')
    contact_user = models.ForeignKey(User)
    is_blocked = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    has_accepted = models.BooleanField(default=False)
    unfriended = models.BooleanField(default=False)
    added_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'user_contact'
        unique_together = ('user', 'contact_user',)


class UserProfileImage(models.Model):
    IMAGE_TYPES = (
        (USER_PROFILE_IMAGE_IMAGE, 'PROFILE_IMAGE'),
        (USER_PROFILE_IMAGE_BACKGROUND, 'PROFILE_BACKGROUND'),
    )
    user = models.ForeignKey(User, related_name='user_profile_images')
    image_type = models.PositiveSmallIntegerField(choices=IMAGE_TYPES)
    image = models.CharField(max_length=255)

    class Meta:
        db_table = 'user_profile_image'


class UserAddress(models.Model):
    user = models.ForeignKey(User, related_name='user_address')
    street = models.CharField(max_length=100)
    suite = models.CharField(max_length=16)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=2, choices=US_STATES)
    zip = models.CharField(max_length=16)
    country = models.CharField(max_length=16)

    class Meta:
        db_table = 'user_address'
        verbose_name_plural = 'user addresses'

    def __unicode__(self):
        return self.user.get_full_name()


class UserTagManager(BaseUserManager):

    def assign_tag_to_user(self, user, assigned_by_user, tag_name, tag_type):
        """
        Creates and assigns a tag to a user
        """
        from tags.models import Tag

        tag, created = Tag.objects.get_or_create(
            tag_name=tag_name,
            tag_type=tag_type,
            defaults={'user': assigned_by_user},
        )

        # get the tag
        return UserTag.objects.create(user=user, assigned_by_user=assigned_by_user, tag=tag)

        
class UserTag(models.Model):
    from tags.models import Tag
    user = models.ForeignKey(User, related_name='user_tags')
    tag = models.ForeignKey(Tag)
    assigned_by_user = models.ForeignKey(User)
    assigned_date = models.DateTimeField(auto_now_add=True)

    objects = UserTagManager()

    class Meta:
        db_table = 'user_tag'
        unique_together = ('user', 'tag', 'assigned_by_user')


class UserTagDeleted(models.Model):
    from tags.models import Tag
    user = models.ForeignKey(User, related_name='user_tags_deleted')
    tag = models.ForeignKey(Tag)
    deleted_date = models.DateTimeField(auto_now_add=True)

class Meta:
        db_table = 'user_tag_deleted'
