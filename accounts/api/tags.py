# -----------------------------------------------------------------------------
# products/views/api.py
#
# (C) Copyright 2015-2017, Pjs Midnight Labs.  All rights reserved.
#
# Author: Pauljames "The Juggernaut" Dimitriu
# -----------------------------------------------------------------------------
from pprint import pprint

from rest_framework.permissions import AllowAny
from rest_framework.parsers import JSONParser, MultiPartParser
from rest_framework import status
from rest_framework import generics, permissions
from rest_framework.response import Response

from django.contrib.auth import authenticate


from accounts.libs.exceptions import DuplicateAssignedUserTagException

from accounts.serializers import (
    UserSerializer,
    UserAssignTag,
    UserTagOutputSerializer,
)


class AssignTagToUser(generics.CreateAPIView):
    serializer_class = UserAssignTag
    http_method_names = ['post',]

    permission_classes = (AllowAny,)
    parser_classes = (JSONParser,  MultiPartParser,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        try:
            serializer.is_valid()
            user_tag = serializer.save()
            output_serializer = UserTagOutputSerializer(user_tag)
            return Response(output_serializer.data, 200)
        except DuplicateAssignedUserTagException as e:
            return Response(e.message, 400) 
