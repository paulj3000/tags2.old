# -----------------------------------------------------------------------------
# products/views/api.py
#
# (C) Copyright 2015-2017, Pjs Midnight Labs.  All rights reserved.
#
# Author: Pauljames "The Juggernaut" Dimitriu
# -----------------------------------------------------------------------------
from pprint import pprint

from rest_framework.permissions import AllowAny
from rest_framework.parsers import JSONParser, MultiPartParser
from rest_framework import status
from rest_framework import generics, permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from django.contrib.auth import authenticate

from accounts.models import User

from accounts.serializers import (
    UserProfileImage,
)


class UploadProfileImage(APIView):

    MODES = {
        'image': 'set_profile_image',
        'background': 'set_profile_background',
    }

    def put(self, request, userid, mode, format=None):        
        data = request.data
        user = User.objects.get(id=userid)

        fn = getattr(user, self.MODES[mode])
        profile = fn(
            data.get('content-type'),
            data.get('image')
        )

        print " ***** "
        print profile
        return Response({'user': {'profile_image': profile}}, status=status.HTTP_200_OK) 

    def get_queryset(self):
        user_id = self.kwargs.get('userid')
        return User.objects.filter(id=user_id)

    serializer_class = UserProfileImage
    http_method_names = ['put',]

    permission_classes = (AllowAny,)
    parser_classes = (JSONParser,)
