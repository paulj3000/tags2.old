# -----------------------------------------------------------------------------
# products/serializers.py
#
# (C) Copyright 2015-2017, Pjs Midnight Labs.  All rights reserved.
#
# Author: Pauljames "The Juggernaut" Dimitriu
# -----------------------------------------------------------------------------
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.core.exceptions import ValidationError

from rest_framework import serializers
from rest_framework import fields

from accounts.models import User, UserTag
from pprint import pprint


class UserSerializer(serializers.ModelSerializer):

    token = serializers.SerializerMethodField()

    def get_token(self, data):
        return ''

    def get_type(self, obj):
        return obj.get_api_token()

    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'last_name', 'token', 'username',)


class AssignedByUserSerializer(serializers.ModelSerializer):

    def get_type(self, obj):
        return obj.get_api_token()

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name',)


class UserAssignTag(serializers.ModelSerializer):
    tag_name = serializers.CharField(max_length=200)
    tag_type = serializers.IntegerField()

    class Meta:
        model = UserTag
        fields = (
            'user',
            'assigned_by_user',
            'tag_name',
            'tag_type',
        )

    def create(self, validated_data):
        user = validated_data['user']

        return user.add_tag(
            validated_data['assigned_by_user'],
            validated_data['tag_name'],
            validated_data['tag_type'],
        )


class UserTagOutputSerializer(serializers.ModelSerializer):
    tag = serializers.SerializerMethodField()
    assigned_by_user = serializers.SerializerMethodField()

    def get_tag(self, data):
        from tags.serializers import TagSerializer
        return TagSerializer(
            data.tag,
            many=False,
            context=self.context
        ).data

    def get_assigned_by_user(self, data):
        return AssignedByUserSerializer(
            data.assigned_by_user,
            many=False,
            context=self.context
        ).data

    class Meta:
        model = UserTag
        fields = (
            'assigned_by_user',
            'tag',
        )


class UserAssignTagSerializer(serializers.ModelSerializer):

    token = serializers.SerializerMethodField()

    def get_type(self, obj):
        return obj.get_api_token()

    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'last_name', 'token',)


class UserProfileImage(serializers.BaseSerializer):
    content_type = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField()

    def get_content_type(self, data):
        return ''

    def get_image(self, data):
        return ''

    def update(self, validated_data):
        user = validated_data['user']

        return user.add_tag(
            validated_data['assigned_by_user'],
            validated_data['tag_name'],
            validated_data['tag_type'],
        )
