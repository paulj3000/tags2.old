class InvalidUserTagException(Exception):
    pass

class DuplicateAssignedUserTagException(Exception):
    def __init__(self, assigned_by_user, tag_name, tag_type):

        msg = "user id %s already assigned tag %s with type %s" % \
            (
                assigned_by_user.id,
                tag_name,
                tag_type
            ),

        # Call the base class constructor with the parameters it needs
        super(DuplicateAssignedUserTagException, self).__init__(msg)

    pass


class InvalidContentTypeException(Exception):
    pass
