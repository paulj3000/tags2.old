# -----------------------------------------------------------------------------
# products/views/api.py
#
# (C) Copyright 2015-2017, Pjs Midnight Labs.  All rights reserved.
#
# Author: Pauljames "The Juggernaut" Dimitriu
# -----------------------------------------------------------------------------
from pprint import pprint

from rest_framework.permissions import AllowAny
from rest_framework.parsers import JSONParser, FormParser
from rest_framework import status
from rest_framework import generics, permissions
from rest_framework.response import Response

from django.contrib.auth import authenticate

from accounts.serializers import (
    UserSerializer,
)


class LoginUser(generics.RetrieveAPIView):
    serializer_class = UserSerializer
    http_method_names = ['post', 'get',]

    permission_classes = (AllowAny,)
    parser_classes = (JSONParser, FormParser,)

    def post(self, request, *args, **kwargs):

        # make sure we have an email and a password
        email = request.data.get('email')
        password = request.data.get('password')

        # attempt to get the user from the supplied parameters
        user = authenticate(email=email, password=password)

        if user:
            return Response(UserSerializer(user).data)
        else:
            return Response({'error': 'Invalid Login Credentials'}, status=status.HTTP_403_FORBIDDEN)


class CreateUser(generics.CreateAPIView):
    serializer_class = UserSerializer
    http_method_names = ['post',]

    permission_classes = (AllowAny,)
    parser_classes = (JSONParser, FormParser,)

    def post(self, request, *args, **kwargs):

        # let's try to create a user
        try:
            retval = User.objects.create_user_from_api(request.data)
            return Response(retval)
        except ValueError as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
