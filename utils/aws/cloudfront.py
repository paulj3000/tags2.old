# import time # will probably need this when creating signed key

from boto import cloudfront
# from boto.cloudfront import distribution

from kith.settings import AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY,\
    AWS_PRIVATE_KEY, AWS_MOVIE_HTTP_URL, AWS_CLOUDFRONT_MOVIE_URL


class CloudFront:
    def __init__(self):
        self.conn = cloudfront.CloudFrontConnection(
            AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)

    def get_signed_url(self, url):
        distro_settings = AWS_CLOUDFRONT_MOVIE_URL + url

        # get the distribution
        distro = cloudfront.distribution.Distribution(
            connection=self.conn,
            id=distro_settings['id'],
            domain_name=distro_settings['name']
        )

        # now, generate the url and return
        return distro.create_signed_url(
            url, AWS_KEY_ID,
            private_key_file=AWS_PRIVATE_KEY
        )
