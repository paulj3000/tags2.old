from __future__ import unicode_literals

import kith.settings
from boto.sqs import connect_to_region
from boto.sqs.message import Message

from django.conf import settings


class SQS:
    def __init__(self, queue):
        self.conn = connect_to_region(
            settings.AWS_S3_REGION,
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY
        )

        # get the queue
        self.queue  = self.conn.get_queue(queue)

    def add_to_queue(self, data):
        m   = Message()
        m.set_body(data)
        self.queue.write(m)

    def get_messages(self):
        return self.queue.get_messages()

    def delete_message(self, m):
        self.queue.delete_message(m)
