import re
from glob import glob
import os
import errno

from boto.s3.key import Key
from boto.s3.connection import S3Connection, OrdinaryCallingFormat
from boto.s3 import connect_to_region

from tags2.settings import AWS_ACCESS_KEY_ID, \
    AWS_SECRET_ACCESS_KEY, AWS_S3_REGION


class S3:
    def __init__(self, bucket_name):
        '''
        self.conn = S3Connection(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY,
            calling_format=OrdinaryCallingFormat())
        self.bucket = self.conn.get_bucket(bucket_name)
        '''

        self.conn = connect_to_region(
            AWS_S3_REGION,
            aws_access_key_id=AWS_ACCESS_KEY_ID,
            aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
            calling_format=OrdinaryCallingFormat()
        )

        self.bucket = self.conn.get_bucket(bucket_name)

    def upload_from_filename(self, fromname, toname, **kwargs):
        ''' upload a file from the file name
        Params:
            fromname: the name of the file
            toname: what to name the file on S3
            headers: Any headers to set
        '''

        # create a key object
        k = Key(self.bucket)
        k.key = toname

        # get any potential headers
        headers = kwargs.get('headers')

        if headers:
            if type(headers) is dict and headers.get('Content-Type'):
                k.set_metadata("Content-Type", headers.get('Content-Type'))

        # get a potential callback
        cb = kwargs.get('cb')

        # set the content
        k.set_contents_from_filename(fromname, cb=cb)

        # and make it public
        k.make_public()

    def upload_file(self, name, fileobj, **kwargs):
        k = Key(self.bucket)
        k.key = name
    
        # look for content type
        if kwargs.get('content_type'):
            k.set_metadata("Content-Type", kwargs['content_type'])

        # get a potential callback
        cb = kwargs.get('cb')

        k.set_contents_from_string(fileobj, cb=cb)
        k.make_public()
        return k

    def delete_file(self, filename):
        self.bucket.delete_key(filename)

    def get_file(self, filename):
        self.bucket.delete_key(filename)

    def list_files(self, prefix=''):
        file_list = []
        for key in self.bucket.list(prefix=prefix):
            file_list.append(key.name.encode('utf-8'))

        return file_list

    def dump_all_files(self, dir):
        # first, generate the directories
        for key in self.bucket.list(prefix=''):
            keyString = str(key.key)
            dest = dir + keyString

            if not re.search(r'\/$', keyString):
                destpath = os.path.dirname(dest)
                print "Creating %s" % destpath
                try:
                    os.makedirs(destpath)
                except OSError as exc:
                    if exc.errno == errno.EEXIST and os.path.isdir(destpath):
                        pass

                print "%s -> %s" % (keyString, dest)
                key.get_contents_to_filename(dest)

    def bulk_upload_files(self, dir):
        # first, generate the directories
        for n in [y for x in os.walk(dir) for y in glob(os.path.join(x[0], '*'))]:
            print n
            if not os.path.isdir(n):
                fname = n.replace(dir, '')
                self.upload_from_filename(n, fname)
