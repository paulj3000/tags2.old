from accounts.models import User
from rest_framework import authentication
from rest_framework import exceptions

class DevAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        username = 'paulj1999@yahoo.com'
        if not username:
            return None
        try:
            user = User.objects.get(email=username)
        except User.DoesNotExist:
            raise exceptions.AuthenticationFailed('No such user')

        return (user, None)
