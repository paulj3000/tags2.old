# -----------------------------------------------------------------------------
# utils/mail.py
#
# Create some mail wrappers so we can abstract most of the mailing options
# (from, bcc, etc)
#
# (C) Copyright 2015, Digital Infinity Software.  All rights reserved.
#
# Author: Pauljames "The Juggernaut" Dimitriu
# -----------------------------------------------------------------------------
from django.core.mail import EmailMultiAlternatives
from ac.settings import EMAIL_FROM, EMAIL_BCC

import mailchimp


def get_mailchimp_api():
    return mailchimp.Mailchimp(MAILCHIMP_API_KEY)


def send_mail(to, subject, html_content, text_content=""):
    msg = EmailMultiAlternatives(subject, text_content,
                                 EMAIL_FROM, [to], bcc=[EMAIL_BCC])

    msg.attach_alternative(html_content, "text/html")
    msg.send()


def subscribe(first_name, last_name, email):
    try:
        m = get_mailchimp_api()
        return m.lists.subscribe(MAILCHIMP_LIST, {'email': email},
                                 {'FNAME': first_name, 'LNAME': last_name},
                                 'html', False, False, True, True)

    except mailchimp.ListAlreadySubscribedError:
        return search_member(email)
    except mailchimp.Error, e:
        return 'An error occurred: %s - %s' % (e.__class__, e)


def unsubscribe(euid):
    try:
        m = get_mailchimp_api()
        return m.lists.unsubscribe(MAILCHIMP_LIST,
                                   {'euid': euid}, True, False, False)

    except mailchimp.ListAlreadySubscribedError:
        return "That email is already subscribed to the list"
    except mailchimp.Error, e:
        return 'An error occurred: %s - %s' % (e.__class__, e)


def store_email(user, email_rendered, title):
    ''' store an email on AWS, give us a way to see what we're sending
    Params:
        user: The user object
        email_rendered: The actual email rendered
        title: a title / description of what we are sending
    '''
    from utils.aws.s3 import S3
    import time
    s3 = S3(AWS_S3_BUCKET_PRIVATE)

    # generate the key name
    file = s3.upload_file('%d/emailsent/%s_%d.html' %
                          (user.id, title, int(time.time())),
                          email_rendered,
                          headers={'Content-Type': 'text/html'})
    file.make_public()
