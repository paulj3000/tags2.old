from pprint import pprint

from ac import settings


def ac(request):
    """
    populate the header and footer fields of the template
    """
    user = request.user

    context = {
        'user': user,
        'company_name': settings.COMPANY_NAME,
    }

    # here is the context
    return context
